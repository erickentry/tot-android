package com.renseki.app.totandroid.ui

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.ComponentNameMatchers.hasShortClassName
import android.support.test.espresso.intent.matcher.IntentMatchers.*
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.runner.AndroidJUnit4
import com.renseki.app.totandroid.R
import com.renseki.app.totandroid.commons.SingleInputActivity
import com.renseki.app.totandroid.ui.aboutme.AboutMeActivity
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AboutMeActivityTest {

    @get:Rule
    val activity = IntentsTestRule(AboutMeActivity::class.java)

    @Test
    fun load_successful() {
        // Espresso
        onView(
            withId(R.id.tv_name)
        )
            .perform(
                click()
            )

        Intents.intended(
            allOf(
                hasComponent(
                    hasShortClassName(
                        ".commons.SingleInputActivity"
                    )
                ),
                toPackage("com.renseki.app.totandroid"),
                hasExtra(SingleInputActivity.EXTRA_VALUE, "Ericks")
            )
        )
    }
}
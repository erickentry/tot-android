package com.renseki.app.totandroid.repository

import android.arch.lifecycle.LiveData
import com.renseki.app.totandroid.model.Me
import com.renseki.app.totandroid.service.MeLocalService

open class MeRepository(
    private val meLocalService: MeLocalService
) {
    open fun load(): LiveData<Me> {
        return meLocalService.load()
    }

    fun persist(me: Me) = meLocalService.persist(me)
}
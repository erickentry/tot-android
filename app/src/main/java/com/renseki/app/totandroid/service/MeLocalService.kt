package com.renseki.app.totandroid.service

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.renseki.app.totandroid.MyApp
import com.renseki.app.totandroid.R
import com.renseki.app.totandroid.model.Me

open class MeLocalService(
    private val context: Context
) {
    open fun load(): LiveData<Me> {
        return MutableLiveData<Me>().apply {
            value = Me(
                getPersistedName("Erick Pranata"),
                "https://pbs.twimg.com/profile_images/3700805240/1b5e372301990dcebaa114522745982c.jpeg",
                "erick@stts.edu",
                "0812345678"
            )
        }
    }

    private fun getPersistedName(defaultName: String): String {
        val sharedPref = context.getSharedPreferences(
            MyApp.SHARED_PREF_FILE_NAME,
            Context.MODE_PRIVATE
        )
        return sharedPref.getString(
            context.getString(R.string.key_my_name),
            defaultName
        ) ?: defaultName
    }

    fun persist(me: Me): LiveData<Me> {
        return MutableLiveData<Me>().apply {
            context
                .getSharedPreferences(
                    MyApp.SHARED_PREF_FILE_NAME,
                    Context.MODE_PRIVATE
                )
                .edit()
                .apply {
                    putString(
                        context.getString(R.string.key_my_name),
                        me.name
                    )
                    apply()
                }
            value = me.copy()
        }
    }
}
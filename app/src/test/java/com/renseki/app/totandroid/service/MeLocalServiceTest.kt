package com.renseki.app.totandroid.service

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.content.Context
import android.content.SharedPreferences
import com.renseki.app.totandroid.util.mock
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify

class MeLocalServiceTest {

    @Rule
    @JvmField
    val instant = InstantTaskExecutorRule()

    private val context: Context = mock()

    @Test
    fun `load me, success, and get value`() {

        val sharedPref: SharedPreferences = mock()

        `when`(
            context.getSharedPreferences(
                anyString(),
                anyInt()
            )
        )
            .thenReturn(sharedPref)

        `when`(
            sharedPref.getString(anyString(), anyString())
        )
            .thenReturn("nama yang diharapkan")

        `when`(
            context.getString(anyInt())
        )
            .thenReturn("some key")

        val service = MeLocalService(context)
        val result = service.load()

        result.observeForever(mock())

        val me = result.value
        assert(me?.name == "nama yang diharapkan")

        verify(sharedPref).getString("some key", "Erick Pranata")
    }
}
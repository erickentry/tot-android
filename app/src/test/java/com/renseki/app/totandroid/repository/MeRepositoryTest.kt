package com.renseki.app.totandroid.repository

import com.renseki.app.totandroid.service.MeLocalService
import com.renseki.app.totandroid.util.mock
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify

class MeRepositoryTest {
    @Test
    fun `load success`() {
        val service: MeLocalService = mock()

        `when`(service.load()).thenReturn(mock())

        val repo = MeRepository(service)
        val result = repo.load()

        Assert.assertNotNull(result)
        verify(service).load()
    }
}
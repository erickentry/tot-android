package com.renseki.app.totandroid.util

import org.mockito.Mockito

inline fun <reified T> mock() = Mockito.mock(T::class.java)
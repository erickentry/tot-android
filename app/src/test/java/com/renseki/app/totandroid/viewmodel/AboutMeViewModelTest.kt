package com.renseki.app.totandroid.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import com.renseki.app.totandroid.model.Me
import com.renseki.app.totandroid.repository.MeRepository
import com.renseki.app.totandroid.ui.aboutme.AboutMeViewModel
import com.renseki.app.totandroid.util.mock
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.`when`

class AboutMeViewModelTest {

    @Rule
    @JvmField
    val instant = InstantTaskExecutorRule()

    @Test
    fun `initial data - modifiedMe comes from repository`() {
        val me = Me(
            name = "name",
            imageUrl = "404",
            email = "email@email.com",
            phone = "0812345"
        )

        val repository: MeRepository = mock()
        val liveDataBohongan = MutableLiveData<Me>().apply {
            value = me
        }
        `when`(repository.load())
            .thenReturn(liveDataBohongan)

        val viewModel = AboutMeViewModel(repository)
        viewModel.modifiedMe.observeForever(mock())
        assert(viewModel.modifiedMe.value == me)
    }
}